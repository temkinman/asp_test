﻿#nullable disable
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TestApp.Models;

namespace TestApp.Controllers
{
    public class JuicesController : Controller
    {
        private readonly JuiceContext _context;

        public JuicesController(JuiceContext context)
        {
            _context = context;
        }

        // GET: Juices
        public async Task<IActionResult> Index()
        {
            var juiceContext = _context.Juices.Include(j => j.Company);
            return View(await juiceContext.ToListAsync());
        }

        // GET: Juices/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var juice = await _context.Juices
                .Include(j => j.Company)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (juice == null)
            {
                return NotFound();
            }

            return View(juice);
        }

        // GET: Juices/Create
        public IActionResult Create()
        {
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name");
            return View();
        }

        // POST: Juices/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,CompanyId, Price")] Juice juice)
        {
            if (ModelState.IsValid)
            {
                _context.Add(juice);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Name", juice.CompanyId);
            return View(juice);
        }

        // GET: Juices/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var juice = await _context.Juices.FindAsync(id);
            if (juice == null)
            {
                return NotFound();
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Id", juice.CompanyId);
            return View(juice);
        }

        // POST: Juices/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,CompanyId,Price")] Juice juice)
        {
            if (id != juice.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(juice);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!JuiceExists(juice.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyId"] = new SelectList(_context.Companies, "Id", "Id", juice.CompanyId);
            return View(juice);
        }

        // GET: Juices/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var juice = await _context.Juices
                .Include(j => j.Company)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (juice == null)
            {
                return NotFound();
            }

            return View(juice);
        }

        // POST: Juices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var juice = await _context.Juices.FindAsync(id);
            _context.Juices.Remove(juice);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool JuiceExists(int id)
        {
            return _context.Juices.Any(e => e.Id == id);
        }
    }
}
