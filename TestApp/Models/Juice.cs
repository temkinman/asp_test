﻿using System.ComponentModel.DataAnnotations;

namespace TestApp.Models
{
	public class Juice
	{
		[Key]
		public int Id { get; set; }
		public string Name { get; set; }
		public int? CompanyId { get; set; }
		public Company Company { get; set; }
		public decimal Price { get; set; }
	}
}
