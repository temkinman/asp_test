﻿using System.ComponentModel.DataAnnotations;

namespace TestApp.Models
{
	public class Company
	{
		[Key]
		public int Id { get; set; }
		public string? Name { get; set; }
	}
}
