﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApp.Models;
using Microsoft.EntityFrameworkCore;

namespace TestApp.Models
{
    public class JuiceContext : DbContext
    {
        public DbSet<Juice> Juices { get; set; }
        public DbSet<Company> Companies { get; set; }

        public JuiceContext(DbContextOptions<JuiceContext> options)
            : base(options)
        {
            //Database.EnsureCreated(); //если бд не существует, то она создается с помощью этого метода
            Database.Migrate();
        }
    }
}
