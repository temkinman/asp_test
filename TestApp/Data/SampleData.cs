﻿using TestApp.Models;

namespace TestApp.Data
{
	public class SampleData
	{
		internal static void Initialize(JuiceContext context)
		{
            if (!context.Juices.Any())
            {
                context.Juices.AddRange(
                    new Juice
                    {
                        Name = "Яблочный",
                        Company = new Company() { Name = "Добрый" }
                    },
                    new Juice
                    {
                        Name = "Мультифруктовый",
                        Company = new Company() { Name = "АВС" }
                    }
                );
                context.SaveChanges();
            }
        }
	}
}
